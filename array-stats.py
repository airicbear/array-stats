#!/usr/bin/env python3

num_digits = lambda x: len(str(x))
bar = lambda l,n: ''.join(['*' for _ in range(l.count(n))])
row = lambda l,n: str(n).rjust(num_digits(max(l))) + ' | ' + bar(l,n)
histogram = lambda l: '\n'.join([row(l,n) for n in sorted(list(set(arr)))])
mean = lambda l: float(sum(l)) / len(l)
mode = lambda l: [x for x in set(l) if l.count(x) == l.count(max(set(l), key=l.count))]
sqrt = lambda x: x ** 0.5
variance = lambda l: sum([(x - mean(l)) ** 2 for x in l]) / (len(l) - 1)
stddev = lambda l: sqrt(variance(l))
pop_variance = lambda l: mean([(x - mean(l)) ** 2 for x in l])
pop_stddev = lambda l: sqrt(pop_variance(l))

def median(l):
  l = sorted(l)
  if len(l) % 2 == 0:
    middle = [l[len(l) // 2], l[len(l) // 2 - 1]]
    return mean(middle)
  return l[len(l) // 2]

def get_int(prompt):
  get_input = str(input(prompt))
  while not get_input.lstrip('-').isdigit():
    get_input = str(input('Invalid argument. Try again: '))
  return int(get_input)

if __name__ == '__main__':
  N = 0
  while N < 2:
    N = get_int('Length of numbers list: ')
  arr = [get_int('Enter an integer: ') for _ in range(N)]
  sep = lambda n: ''.join(['=' for _ in range(n)])
  sep_len = 70

  print(sep(sep_len))
  print('Mean: ' + str(mean(arr)))
  print('Median: ' + str(median(arr)))
  print('Mode: ' + ', '.join(map(str, mode(arr))))
  print('Min: ' + str(min(arr)))
  print('Max: ' + str(max(arr)))
  print('Variance: ' + str(variance(arr)))
  print('Standard Deviation: ' + str(stddev(arr)))
  print('Population Variance: ' + str(pop_variance(arr)))
  print('Population Standard Deviation: ' + str(pop_stddev(arr)))
  print(histogram(arr))
  print(sep(sep_len))
